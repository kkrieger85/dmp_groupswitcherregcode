<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;

$rule = Mage::getModel('GroupSwitcher/rule');
/* @var $rule DerModPro_GroupSwitcher_Model_Rule */

$rule->setName('GroupSwitcher Registration Codes')
	->setRuleType('customer_attribute')
	->setIsActive(1)
	->setPriority(1)
	->setStopProcessing(1)
	->setRuleValue1('groupswitcher_regcode')
	->setRuleValue2('GroupSwitcherRegCode/observer::applyRule')
	->setRuleValue3('callback')
	->save()
	;
