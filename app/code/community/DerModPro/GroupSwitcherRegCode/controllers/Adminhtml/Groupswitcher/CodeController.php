<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

class DerModPro_GroupSwitcherRegCode_Adminhtml_Groupswitcher_CodeController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$helper = Mage::helper('GroupSwitcherRegCode');

		$this->_title($helper->__('Customers'))->_title($helper->__('Group Switcher'))->_title($helper->__('Codes'));

		$this->loadLayout();

		$this->_setActiveMenu('customer/group/groupswitcher_code');

		$this->_addBreadcrumb($helper->__('Customers'), $helper->__('Customers'));
		$this->_addBreadcrumb($helper->__('Group Switcher'), $helper->__('Group Switcher'));
		$this->_addBreadcrumb($helper->__('Codes'), $helper->__('Codes'));

		$this->renderLayout();
	}

	public function newAction()
	{
		$this->_forward('edit');
	}

	public function editAction()
	{
		$helper = Mage::helper('GroupSwitcherRegCode');
		$model = Mage::getModel('GroupSwitcherRegCode/code');
		$id = $this->getRequest()->getParam('id');
		$errorRedirectPath = '*/*';

		try
		{
			if ($id)
			{
				if (! $model->load($id)->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcherRegCode')->__('No record with ID "%s" found.', $id));
				}
			}

			Mage::register('groupswitcher_code', $model);

			/*
			 * Build the page title
			 */
			if ($model->getId())
			{
				$pageTitle = $helper->__('Edit Code #%d', $model->getId());
			}
			else
			{
				$pageTitle = $helper->__('New Code');
			}
			$this->_title($helper->__('Customers'))->_title($helper->__('Group Switcher'))->_title($pageTitle);

			$this->loadLayout();

			$this->_addBreadcrumb($helper->__('Customers'), $helper->__('Customers'));
			$this->_addBreadcrumb($helper->__('Group Switcher'), $helper->__('Group Switcher'));
			$this->_addBreadcrumb($pageTitle, $pageTitle);

			$this->renderLayout();
		}
		catch (Mage_Core_Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect($errorRedirectPath);
		}
		catch (Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect($errorRedirectPath);
		}
	}

	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost())
		{
			$data['id'] = $this->getRequest()->getParam('id');
			$this->_getSession()->setFormData($data);

			$model = Mage::getModel('GroupSwitcherRegCode/code');

			$id = $this->getRequest()->getParam('id');

			try
			{
				if ($id) $model->load($id);
				$model->addData($data)->save();

				if (! $model->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcherRegCode')->__('Error saving code'));
				}

				$this->_getSession()->addSuccess(Mage::helper('GroupSwitcherRegCode')->__('Code was successfully saved'));
				$this->_getSession()->setFormData(false);

				if ($this->getRequest()->getParam('back'))
				{
					$params = array('id' => $model->getId());
					if ($activeTab = $this->getRequest()->getParam('active_tab'))
					{
						$params['active_tab'] = $activeTab;
					}
					$this->_redirect('*/*/edit', $params);
				}
				else
				{
					$this->_redirect('*/*/');
				}
			}
			catch (Exception $e)
			{
				$this->_getSession()->addError($e->getMessage());
				if ($model && $model->getId())
				{
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
				}
				else
				{
					$this->_redirect('*/*/new');
				}
			}

			return;
		}

		$this->_getSession()->addError(Mage::helper('GroupSwitcherRegCode')->__('No data found to save'));
		$this->_redirect('*/*/');
	}

	public function deleteAction()
	{
		$model = Mage::getModel('GroupSwitcherRegCode/code');
		$id = $this->getRequest()->getParam('id');

		try
		{
			if ($id)
			{
				if (! $model->load($id)->getId())
				{
					Mage::throwException(Mage::helper('GroupSwitcherRegCode')->__('No record with ID "%s" found.', $id));
				}
				$model->delete();
				$this->_getSession()->addSuccess(Mage::helper('GroupSwitcherRegCode')->__('Code %d was successfully deleted', $id));
				$this->_redirect('*/*');
			}
		}
		catch (Mage_Core_Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect('*/*');
		}
		catch (Exception $e)
		{
			Mage::logException($e);
			$this->_getSession()->addError($e->getMessage());
			$this->_redirect('*/*');
		}
	}
}

