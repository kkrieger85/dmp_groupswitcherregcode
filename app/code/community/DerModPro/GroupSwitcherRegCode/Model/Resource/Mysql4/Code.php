<?php

class DerModPro_GroupSwitcherRegCode_Model_Resource_Mysql4_Code extends Mage_Core_Model_Mysql4_Abstract
{
	protected function  _construct()
	{
		$this->_init('GroupSwitcherRegCode/reg_codes', 'id');
	}

	public function loadByCode(DerModPro_GroupSwitcherRegCode_Model_Code $model, $code)
	{
		$model->setData(array())->setId(null);
		if ($id = $this->getIdByCode($code))
		{
			$this->load($model, $id);
		}
		return $this;
	}

	public function getIdByCode($code)
	{
		$select = $this->getReadConnection()->select()
				->from($this->getMainTable(), 'id')
				->where('code = ?', $code);
		if ($id = $this->getReadConnection()->fetchOne($select))
		{
			return $id;
		}
		return false;
	}

	public function getUsageCount(DerModPro_GroupSwitcherRegCode_Model_Code $code)
	{
		$attribute = Mage::getModel('customer/attribute');
		/* @var $attribute Mage_Customer_Model_Attribute */
		$attribute->loadByCode('customer', 'groupswitcher_regcode');

        $select = $this->_getReadAdapter()->select()
            ->from(array('used_codes' => $attribute->getBackendTable()), array(
                'usage_count' => new Zend_Db_Expr('COUNT(*)'),
            ))
			->where('attribute_id=?', $attribute->getId())
			->where('value=?', $code->getCode());

		$count = $this->_getReadAdapter()->fetchOne($select);

		return $count;
	}
}
